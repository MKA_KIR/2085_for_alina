import {useTranslation} from "next-i18next";

import Image from "next/image";

import styles from './basket.module.scss';

const Basket = () => {
    const {t} = useTranslation('basket')
    return (
            <div className={styles.basket}>
                <div className={styles.basket_wrap}>
                    <div className={styles.basket_wrap_header}>
                        <div className={styles.basket_cart}>
                            <Image className={styles.basket_cart_img} src="/img/beer/2085-1.png" alt="2085-1 Bohemian Pilsner" layout="fill" />
                            <div className={styles.basket_cart_title}></div>
                            <div className={styles.basket_cart_price}>UAH</div>
                            <div className={styles.basket_cart_counter}></div>
                            <div className={styles.basket_cart_del}></div>
                        </div>
                    </div>
                    <div className={styles.basket_wrap_body}>
                        <div className={styles.basket_wrap_body_title}></div>
                        <div className={styles.basket_wrap_body_total_price}></div>
                    </div>
                    <button className={styles.basket_wrap_btn}>Оформити замовлення</button>
                </div>
            </div>
    );
};

export default Basket;
