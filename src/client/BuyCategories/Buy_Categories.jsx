import {useRouter} from "next/router";
import { useTranslation } from 'next-i18next'

import Custom_link from "../../shared/components/CustomLink";

import styles from "../../../styles/pages/buy.module.scss";

const Buy_Categories = () => {

    const { t } = useTranslation('buy-categories')

    const {pathname} = useRouter()

    return (
            <div className={styles.link_container}>
                <Custom_link href={"/buy/buy"} text={t("beer")} className={pathname === '/buy' ? styles.link_active : styles.link}/>
                <Custom_link href={"/buy/merch"} text={t("merch")} className={pathname === '/merch' ? styles.link_active : styles.link}/>
            </div>
    );
};

export default Buy_Categories;
