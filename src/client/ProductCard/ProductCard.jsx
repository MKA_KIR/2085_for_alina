import PropTypes from 'prop-types';

import Link from "next/link";
import Image from "next/image";

import styles from "./productCard.module.scss";

const ProductCard = ({name, image, price, id}) => {
    return (
        <Link href={`/buy/${id}`}>
            <div className={styles.card}>
                <div className={styles.img}>
                    <Image src={image} alt={name} layout="fill"/>
                </div>
                <h4 className={styles.name}>{name}</h4>
                <p className={styles.price}>{price} UAH</p>
            </div>
        </Link>
    );
};

ProductCard.propTypes = {
    name: PropTypes.string.isRequired,
    image: PropTypes.string,
    price: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired

}
export default ProductCard;
