import ticker_styles from "./ticker.module.scss";
import Ticker_link from "./TickerLink/Ticker_link";

const Ticker = () => {
    return (
        <div className={ticker_styles.text}>
            <div className={ticker_styles.ticker}>
            <Ticker_link/>
            </div>
        </div>
    );
};

export default Ticker;
