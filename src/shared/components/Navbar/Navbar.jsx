import {useRouter} from 'next/router';
import {useTranslation} from "next-i18next";

import Custom_link from "../CustomLink";
import Ticker from "./Ticker";
import Basket from "../../../client/Basket";
import Link from "next/link";

import styles from "./navbar.module.scss";

const Navbar = () => {
    const router = useRouter()
    const {t} = useTranslation('navbar')

    return (
        <div className={styles.navbar}>
            {/*<Basket/>*/}
            <div className={styles.navbar_menu}>
                <Link className={styles.navbar_lang} href={router.route} locale={router.locale === 'ua' ? 'en' : 'ua'}>
                    <button className={styles.btn_language}>
                        {router.locale === 'ua' ? 'en' : 'ua'}
                    </button>
                </Link>
                <Custom_link href={"/"} text="2085" className={styles.logo}/>
                <div className={styles.navbar_links}>
                    <Custom_link href={"/contacts"} text={t("contacts")} className={styles.navbar_link}/>
                    <Custom_link href={"/buy"} text={t("buy")} className={styles.navbar_link}/>
                    <Custom_link href={"/basket"} text={t("0")} className={styles.navbar_link}/>
                </div>
            </div>
            <Ticker/>
        </div>
    );
};

export default Navbar;
