import Link from "next/link";
import PropTypes from "prop-types";

export default function Custom_link({text, href, className}){
    return(
        <Link href={href}>
            <a className={className}>{text}</a>
        </Link>
    )
}
Custom_link.defaultProps = {href: "#"}

Custom_link.propTypes ={
    text: PropTypes.string.isRequired,
    href: PropTypes.string,
    className: PropTypes.string,

}
