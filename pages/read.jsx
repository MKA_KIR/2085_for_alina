import Image from 'next/image';

import Layout from "../src/shared/components/Layout";

import styles from "../styles/pages/read.module.scss";

import parse from 'html-react-parser';
import profilePic from '../public/img/read_image.jpg'
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

import {useTranslation} from "next-i18next";

const Read = () => {

    const { t } = useTranslation('read')
    return (
        <Layout title={t("title")} description={t("description")}>
            <div className={styles.read}>
                <div>
                    <div className={styles.read_img}>
                        <Image src={profilePic} alt="Picture of the beer"/>
                    </div>
                </div>
                <div className={styles.read_column_text}>
                    {parse(t("readText"))}
                </div>
            </div>
        </Layout>
    );
};

export default Read;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar','read']),
    },
})
