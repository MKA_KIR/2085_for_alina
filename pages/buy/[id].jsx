import PropTypes from 'prop-types';
import styles from "../../styles/pages/singleBeerPage.module.scss";

import Image from "next/image";
import Layout from "../../src/shared/components/Layout";
import {useTranslation} from "next-i18next";

const SingleBeerPage = ({name, image, price, code, composition, description}) => {

    const { t } = useTranslation('SingleBeerPage')
    return (
        <Layout title={t("title")} description={t("description")}>
            <div className={styles.SingleBeerPage}>
                <div className={styles.img}>
                    <Image src="/img/beer/2085-1.png" alt="2085-1 Bohemian Pilsner" layout="fill" />
                </div>
                <h4 className={styles.name}>2085-1 Bohemian Pilsner</h4>
                <p className={styles.code}>ABV 6.9 IBU 25</p>
                <p className={styles.composition}>вода, ячмінний солод, хміль, дріжджі</p>
                <p className={styles.description}>Класичний Чеський пільзнер з яскраво вираженою гірчинкою, легким тілом та помірним дріжджовим посмаком. Хміль Cааз надає благородні аромати сіна та тютюну.</p>
                <p className={styles.price}>52 UAH</p>
                <button className={styles.btn}>Купити</button>
            </div>
        </Layout>
    );
};

SingleBeerPage.propTypes ={
    name: PropTypes.string.isRequired,
    image: PropTypes.string,
    price: PropTypes.number.isRequired,
    code: PropTypes.string.isRequired,
    composition: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired

}
export default SingleBeerPage;
