import Layout from "../../src/shared/components/Layout";
import Buy_Categories from "../../src/client/BuyCategories";
import Product_list from "../../src/client/ProductList";

import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useTranslation} from "next-i18next";

const Index = () => {
    const {t} = useTranslation('product-list')
    const {t: buy} = useTranslation('buy')
    const list = t("list", { returnObjects: true })

    return (
            <Layout title={buy("title")} description={buy("description")}>
                <Buy_Categories/>
                <Product_list list={list}/>
            </Layout>
    );
};

export default Index;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'buy-categories', 'product-list', "buy"]),
    },
})
