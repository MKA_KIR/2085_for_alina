import Layout from "../src/shared/components/Layout";
import MainHome from "../src/client/MainHome";


import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import {useTranslation} from "next-i18next";

export default function Home() {
    const {t} = useTranslation("index")
    return (
        <Layout title={t("title")} description={t("description")}>
            <MainHome/>
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'index']),
    },
})
